#!/bin/bash

action=$1
ssid=$2

listnetworks="ls"
connect="con"
disconnect="dis"

validActions='Valid actions are:\n
	\t"ls" to list networks\n
	\t"con $ssid" to connect to a network\n
	\t"dis $ssid" to disconnect from a network'


if [ -z "$action" ]
then
	echo "Specify an action when using this script"
	echo -e $validActions
	exit 1
fi

if [ "$action" = "$listnetworks" ]; then
	nmcli device wifi list
elif [ "$action" = "$connect" ]; then
	nmcli device wifi connect $ssid -ask
elif [ "$action" = "$disconnect" ]; then
	nmcli con down id $ssid
else
	echo "No valid action specified"
	echo -e $validActions
fi
